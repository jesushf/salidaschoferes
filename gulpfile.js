var gulp=require('gulp');
var concat=require('gulp-concat');
var browserSync=require('browser-sync').create();
var scripts = require('./scripts');
var styles = require('./styles');
var devMode = false;
// Concatena todos los archivos css de la ruta dentro del archivo styles.json formando un solo archivo main.css dentro de app/dist/css
gulp.task('css', function(){
	gulp.src(styles)
	.pipe(concat('main.css'))
	.pipe(gulp.dest('./dist/css'))
	.pipe(browserSync.reload({stream:true }));
});
// Concatena todos los archivos js de la ruta dentro del archivo scripts.json formando un solo archivo scripts.js dentro de app/dist/js
gulp.task('js', function(){
	gulp.src(scripts)
	.pipe(concat('scripts.js'))
	.pipe(gulp.dest('./dist/js'))
	.pipe(browserSync.reload({stream:true }));
});
//Mueve todos los templates dentro de app/templates/(subcarpetas y archivos)/(todos los archivos).html a la ruta app/dist
gulp.task('html', function(){
	gulp.src('./app/templates/**/*.html')
	.pipe(gulp.dest('./dist/'))
	.pipe(browserSync.reload({stream:true }));
});
gulp.task('img', function(){
	gulp.src('./app/img/**/*.*')
	.pipe(gulp.dest('./dist/img'))
	.pipe(browserSync.reload({stream:true }));
});
//Es una tarea que incluye las tareas css, js, html cuando corre esta también se corren todas las que están dentro del arreglo
gulp.task('build', function(){
	gulp.start(['css','js','html']);
});
//para sincronizar el código con el navegador web
gulp.task('browser-sync', function(){
	browserSync.init(null,{
		open: false,
		server:{
			baseDir: 'dist'
		}	});
});
gulp.task('default', function(){
	devMode=true;
	gulp.start(['build', 'browser-sync']);
	//El watch busca cambios en la ruta que se le da como 1er argumento y luego corre la tarea del segundo argumento
	gulp.watch(['./app/css/**/*.css'],['css']);
	gulp.watch(['./app/js/**/*.js'],['js']);
	gulp.watch(['./app/templates/**/*.html'],['html']);
});