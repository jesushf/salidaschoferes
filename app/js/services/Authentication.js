/**
 * Created by jesus on 14/03/17.
 */
retail.service('Authentication',['$state', function($state){

       return {
           loginErrorFn: function () {
              alert('Error de inicio de sesión');
            },
           getAuthenticatedAccount: function(){
                     if (!localStorage.getItem('usern')) {
                            return ;
                     }
                     return localStorage.getItem('usern');
              },
              isAuthenticated: function (){
                     return !!localStorage.getItem('usern');
              },
              setAuthenticatedAccount: function (data) {
                     localStorage.setItem('usern', data.data.username);
                     $state.go('retail1');
              },
              unauthenticate: function () {
                     localStorage.removeItem('usern');
                     //$state.go('retail');
              }
       };
}]);

