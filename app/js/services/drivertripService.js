/**
 * Created by jesus on 29/03/17.
 */
retail
    .factory('drivertripService',function($resource, domain) {
        return $resource(
            domain.get+'/driver/trips/',
            {},
            {
                'query': {
                    method: 'GET',
                    isArray: true,
                    headers: {
                        'Content-Type':'application/json'
                    }
                }
            },
            {
                stripTrailingSlashes: false
            }
        );
    });