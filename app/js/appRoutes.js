/**
 * Created by jesus on 16/02/17.
 */
angular.module('appRoutes',['ui.router']).config(['$stateProvider','$urlRouterProvider', function($stateProvider,$urlRouterProvider){
	$stateProvider.state({name: 'retail',
        url: '/',
		templateUrl:('/login.html'),
		controller: 'LoginController',
		reload: true
	}).state({
        name: 'retail25',
        url: '/logout',
        templateUrl: ('/logout.html'),
        controller: 'LogoutController',
		reload: true
    }).state({
        name: 'retail27',
        url: '/drivertrip/detail/:id',
        templateUrl: ('/drivertrips/drivertrip.html'),
        controller: 'IdDriverTripController',
		reload: true
    }).state({
        name: 'retail1',
        url: '/drivertrips',
        templateUrl: ('/drivertrips/all.html'),
        controller: 'DriverTripController',
		reload: true
    }).state( {name: 'retail35',
		url: '/trip/detail/:id',
		templateUrl:('/trips/trip.html'),
		controller: 'IdTripController',
		reload: true
	}).state( {name: 'retail18',url: '/vehicle/detail/:id',
		templateUrl:('/vehicles/vehicle.html'),
		controller: 'IdVehicleController',
		reload: true,
	});

	$urlRouterProvider.otherwise('/');
}]);