/**
 * Created by jesus on 29/03/17.
 */
var $j = jQuery.noConflict();
retail
    .controller('DriverTripController',['$scope','drivertripService','Authentication','$state','domain','$http', function($scope, drivertripService, Authentication,$state,domain,$http) {
      $scope.auth=Authentication.isAuthenticated();
        activate();
    function activate() {
      // If the user is not authenticated, they should not be here.
      if (!$scope.auth) {
        $state.go('retail');
      }
    }
    $scope.user=Authentication.getAuthenticatedAccount();
        $scope.trips = [{}];
        $scope.show= true;
        $scope.not=0;
        $scope.notified = false;
        $scope.notifications=[{}];
        $http({method:'GET', url:domain.get+'/user/notifications/'})
            .then(function(data){
                    $scope.notifications=data.data;
                    if($scope.notifications.length>0){
                        $scope.notified = true;
                        $scope.not=$scope.notifications.length;
                        }
                    else
                        $scope.notified = false;

            });
        $scope.hide=function(){
          $scope.show=!$scope.show;
        };
        $j(document).ready(function()
                    {
                        $j("#notificationLink").click(function()
                    {
                        $j("#notificationContainer").fadeToggle(300);
                        $j("#notification_count").fadeOut("slow");
                        return false;
                    });

                        //Document Click hiding the popup
                        /*$j(document).click(function()

                    {
                        $j("#notificationContainer").hide();
                    });*/

                        //Popup on click
                        $j("#notificationContainer").click(function()
                    {
                        $j("#notificationContainer").hide();
                    });

                    });
    $scope.user=Authentication.getAuthenticatedAccount();
        $scope.drivertrips = [{}];
        drivertripService.query().$promise.then(function(data) {
            $scope.drivertrips = data;
        });
}]);