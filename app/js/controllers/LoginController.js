/**
 * Created by jesus on 14/03/17.
 */
var $j = jQuery.noConflict();
retail.controller('LoginController',['$scope','Authentication','$state','$http','domain', function($scope, Authentication,$state,$http, domain){
    $scope.formData={};
    activate();
    function activate() {
      // If the user is authenticated, they should not be here.
      if (Authentication.isAuthenticated()) {
        $state.go('retail1');
      }
    }
    $scope.send=function(){
           $http({method: 'POST', url:domain.get+'/logindriver/', data:$j.param($scope.formData),
    headers: {'Content-Type': 'application/x-www-form-urlencoded'}  }).then(function() {
                       $http({method:'GET',url:domain.get+'/username/'}).then(function(data){
                          Authentication.setAuthenticatedAccount(data);
                       });
           }).catch(function() {
            Authentication.loginErrorFn();
            });
    };
}]);
//$http({method: 'GET', url:domain.get+'/username/'}).then(function(data){ console.log(data.data); });