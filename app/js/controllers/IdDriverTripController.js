/**
 * Created by jesus on 30/03/17.
 */
retail
    .controller('IdDriverTripController',['$scope','$state', '$http','domain','Authentication', function($scope,$state,$http, domain, Authentication) {
      $scope.auth=Authentication.isAuthenticated();
        activate();
    function activate() {
      // If the user is not authenticated, they should not be here.
      if (!$scope.auth) {
        $state.go('retail');
      }
    }
    $scope.user=Authentication.getAuthenticatedAccount();
        $scope.drivertrip={};
        $http({method:'GET', url:domain.get+'/drivertrips/'+$state.params.id+'/', isArray:false, headers:{'Content-Type':'application/json'}}).then(function(data) {
            $scope.drivertrip = data.data;
            $http({method:'GET', url:domain.get+'/users/'+$scope.drivertrip.driver+'/',isArray:false, headers:{'Content-Type':'application/json'}}).
            then(function(data){
                $scope.driver=data.data.fullName;
            });
        }).catch(function() {
            alert("Error no se pudo completar la operación");
            });
}]);