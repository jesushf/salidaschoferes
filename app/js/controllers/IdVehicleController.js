/**
 * Created by jesus on 17/02/17.
 */
retail
    .controller('IdVehicleController',['$scope','$state', '$http','domain','Authentication', function($scope,$state,$http, domain, Authentication) {
         $scope.auth=Authentication.isAuthenticated();
        activate();
    function activate() {
      // If the user is not authenticated, they should not be here.
      if (!$scope.auth) {
        $state.go('retail');
      }
    }
    $scope.user=Authentication.getAuthenticatedAccount();
        $scope.vehicle={};
        $http({method:'GET', url:domain.get+'/vehicles/'+$state.params.id+'/', isArray:false, headers:{'Content-Type':'application/json'}}).then(function(data) {
            $scope.vehicle = data.data;
        }).catch(function() {
            alert("Error no se pudo completar la operación");
            });

}]);