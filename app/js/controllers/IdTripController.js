/**
 * Created by jesus on 17/02/17.
 */
retail
    .controller('IdTripController',['$scope','$state', '$http','domain','Authentication', function($scope,$state,$http, domain,Authentication) {
        $scope.auth=Authentication.isAuthenticated();
        activate();
    function activate() {
      // If the user is not authenticated, they should not be here.
      if (!$scope.auth) {
        $state.go('retail');
      }
    }
    $scope.user=Authentication.getAuthenticatedAccount();
        $scope.trip={};
        $http({method:'GET', url:domain.get+'/trips/'+$state.params.id+'/', isArray:false, headers:{'Content-Type':'application/json'}}).then(function(data) {
            $scope.trip = data.data;
            $http({method:'GET', url:domain.get+'/departments/'+$scope.trip.department+'/',isArray:false, headers:{'Content-Type':'application/json'}}).
            then(function(data){
                $scope.department=data.data.name;
            });
        }).catch(function() {
            alert("Error no se pudo completar la operación");
            });

}]);