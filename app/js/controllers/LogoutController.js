/**
 * Created by jesus on 22/03/17.
 */
var $j = jQuery.noConflict();
retail.controller('LogoutController',['$scope','Authentication','$state','$http','domain','$cookies','$window', function($scope, Authentication,$state,$http, domain, $cookies,$window){
        if (Authentication.isAuthenticated()) {
           $http({method: 'POST', url:domain.get+'/logout2/'  }).then(function() {
                          Authentication.unauthenticate();
                          $cookies.remove('csrftoken');
                          $state.go('retail');
                       }).catch(function(response) {
                //Authentication.loginErrorFn();
                $window.location.reload();
            });
        }
}]);
//$http({method: 'GET', url:domain.get+'/username/'}).then(function(data){ console.log(data.data); });