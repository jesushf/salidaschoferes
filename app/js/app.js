var retail = angular.module('retail',[]);
var salidasApp = angular.module('salidasApp',['appRoutes','retail','ngResource','mgcrea.ngStrap','ngCookies']);
    salidasApp.run(function run($http, $cookies) {
  $http.defaults.withCredentials = true;
  $http.defaults.xsrfCookieName = 'csrftoken';
  $http.defaults.xsrfHeaderName = 'X-CSRFToken';
  $http.defaults.headers.post['X-CSRFToken'] = $cookies.get('csrftoken');
  });